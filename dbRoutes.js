const NoiseCancellationVideo = require('./models').NoiseCancellationVideo;

module.exports = router => {

    router.get('/count', (req, res) => {
        console.log('hello count')
       NoiseCancellationVideo.count(req.query)
       .then(count => res.json(count))
       .catch(err => {
           console.log(err);
           return res.status(400).send(err.message)
       })
    
    })
    
    // CRUD routes
    router.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest} = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }
    
        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        console.log(rest)
        if (one) {
            q = NoiseCancellationVideo.findOne(rest);
        } else {
            q = NoiseCancellationVideo.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((noiseCancellationVideos) => {
            return res.json(noiseCancellationVideos);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.post('/', (req, res) => {
        const data = req.body;
        NoiseCancellationVideo.create(data)
        .then((noiseCancellationVideo) => {
            return res.json(noiseCancellationVideo);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        NoiseCancellationVideo.update(conditions, { $set: values }, { ...options, multi: true })
        .then(() => NoiseCancellationVideo.find(conditions))
        .then(noiseCancellationVideos => {
            return res.json(noiseCancellationVideos);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.delete('/', (req, res) => {
        let conditions = req.body;
        let noiseCancellationVideos;
        NoiseCancellationVideo.find(conditions)
        .then((a) => {
            noiseCancellationVideos = a;
            return NoiseCancellationVideo.remove(conditions)
        })
        .then(() => {
            return res.json(noiseCancellationVideos);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.get('/:id', (req, res) => {
        NoiseCancellationVideo.findById(req.params.id)
        .then((noiseCancellationVideo) => {
            return res.json(noiseCancellationVideo);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        NoiseCancellationVideo.findByIdAndUpdate(id, { $set: changes })
        .then(() => NoiseCancellationVideo.findById(id))
        .then(noiseCancellationVideo => {
            return res.json(noiseCancellationVideo);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    router.delete('/:id', (req, res) => {
        const { id }  = req.params;
        let deletedNoiseCancellationVideo;
        NoiseCancellationVideo.findById(id)
        .then(noiseCancellationVideo => {
            deletedNoiseCancellationVideo = noiseCancellationVideo;
            return NoiseCancellationVideo.findByIdAndRemove(id)
        })
        .then(() => {
            return res.json(deletedNoiseCancellationVideo);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })
    
    

    return router;
}