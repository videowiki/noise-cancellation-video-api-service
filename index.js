const mongoose = require('mongoose');
const DB_CONNECTION_URL = process.env.NOISE_CANCELLATION_VIDEO_SERVICE_DATABASE_URL;
const rabbitmqService = require('@videowiki/workers/vendors/rabbitmq');
const RABBITMQ_SERVER = process.env.RABBITMQ_SERVER;
const { server, app, createRouter } = require('./generateServer')();
const controller = require('./controller');
const videowikiGenerators = require('@videowiki/generators');

const multer = require('multer');
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, '/tmp')
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.' + file.originalname.split('.').pop())
    }
})
const upload = multer({ storage: storage })
    

let mongoConnection
mongoose.connect(DB_CONNECTION_URL)
.then(con => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })
    rabbitmqService.createChannel(RABBITMQ_SERVER, (err, channel) => {
        if (err) {
            throw err;
        }
        rabbitmqChannel = channel;
        channel.on('error', (err) => {
            console.log('RABBITMQ ERROR', err)
            process.exit(1);
        })
        channel.on('close', () => {
            console.log('RABBITMQ CLOSE')
            process.exit(1);
        })
        require('./rabbitmqHandlers').init(channel)
        // healthcheck route
        videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection, rabbitmqConnection: rabbitmqChannel.connection });
        
        app.use('/db', require('./dbRoutes')(createRouter()));
        
        app.all('*', (req, res, next) => {
            if (req.headers['vw-user-data']) {
                try {
                    const user = JSON.parse(req.headers['vw-user-data']);
                    req.user = user;
                } catch (e) {
                    console.log(e);
                }
            }
            next();
        })
        
        app.get('/', controller.getVideos);
        app.post('/', upload.any(), controller.uploadVideo)
        
        const PORT = process.env.PORT || 4000;
        server.listen(PORT)
        console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
        console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
    })

    
})
.catch(err => {
    console.log('error mongo connection', err);
    process.exit(1);
})