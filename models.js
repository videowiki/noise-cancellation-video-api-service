const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CONVERT_STATUS_ENUM = ['uploaded', 'processing', 'done', 'failed'];

const NoiseCancellationVideoSchema = new Schema({
    url: { type: String },
    noiseCancelledUrl: { type: String },
    title: { type: String },
    Key: { type: String },
    organization: { type: Schema.Types.ObjectId },
    uploadedBy: { type: Schema.Types.ObjectId },
    status: { type: String, enum: CONVERT_STATUS_ENUM, default: 'uploading' },
    created_at: { type: Date, default: Date.now, index: true },
})

const NoiseCancellationVideo = mongoose.model('noiseCancellationVideo', NoiseCancellationVideoSchema)

module.exports = { NoiseCancellationVideo };