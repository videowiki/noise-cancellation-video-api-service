const {
    RABBITMQ_SERVER,
    AUDIO_PROCESSOR_API_ROOT
} = process.env;

const audioProcessor = require('@videowiki/workers/audio_processor')({ RABBITMQ_SERVER, AUDIO_PROCESSOR_API_ROOT })

module.exports = {
    audioProcessor,
}